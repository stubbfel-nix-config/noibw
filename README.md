# NOIBW  - Nix-Openwrt-ImageBuilder-Wrapper

provide a nix shell, for creating openwrt images by the image builder in declarative way.

## buildOpenwrt.nix - Base-Nix-Shell File

This file setup the nix shell with all necessary dependencies and shell environment.

### Parameter

```nix
{
    pkgs ? import <nixpkgs> {}, # nix packages collection
    target, # build target
    arch, # target architecture
    profile, # build profile
    version, # openwrt version
    packages ? [], # list of user installed packages
    manifestPkgs ? "$(cat manifest_packages)", # manifest packages (fetch command)
    workingFolder ? "$PWD/_builder" # working folder
}:
```

### Commands

loadBuilder -> load device specific image builder and manifest ("default") packages list:

```sh
# nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="<target_name>"; arch="<arch_name"; version="<openwrt_release_version>"; profile="<profile_name>"; packages=[<list_of_user_packages>];}' --pure --run loadBuilder
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="brcm2708"; arch="bcm2708"; version="19.07.4"; profile="rpi"; packages=["luci-ssl"];}' --pure --run loadBuilder
```

makeImage -> create image by image builder

```sh
# nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="<target_name>"; arch="<arch_name"; version="<openwrt_release_version>"; profile="<profile_name>"; packages=[<list_of_user_packages>];}' --pure --run makeCmakeImageleanImage
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="brcm2708"; arch="bcm2708"; version="19.07.4"; profile="rpi"; packages=["luci-ssl"];}' --pure --run makeImage
```

removeBuilder -> remove image builder

```sh
# nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="<target_name>"; arch="<arch_name"; version="<openwrt_release_version>"; profile="<profile_name>"; packages=[<list_of_user_packages>];}' --pure --run removeBuilder
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="brcm2708"; arch="bcm2708"; version="19.07.4"; profile="rpi"; packages=["luci-ssl"];}' --pure --run removeBuilder
```

makeCleanImage -> remove old image builder, load new image builder and create image

```sh
# nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="<target_name>"; arch="<arch_name"; version="<openwrt_release_version>"; profile="<profile_name>"; packages=[<list_of_user_packages>];}' --pure --run makeCleanImage
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./buildOpenwrt.nix {target="brcm2708"; arch="bcm2708"; version="19.07.4"; profile="rpi"; packages=["luci-ssl"];}' --pure --run makeCleanImage
```

created image can be found:

```sh
${workingFolder}/openwrt-imagebuilder-${version}-${target}-${arch}.Linux-x86_64/bin/targets/${target}/${arch}
```

## Device File

Instead of using the `--expr` option, you can write the build parameter into a "device file", which describe the target device image.

```nix
{ pkgs ? import <nixpkgs> {}}:
pkgs.callPackage <path_to_repo>/buildOpenwrt.nix {
        target = "<target_name>";
        arch = "<arch_name>";
        version= "<version_number_string>";
        profile = "<profile_name";
        packages = [ <list_of_user_packages>];
}
```

pi example

```nix
{ pkgs ? import <nixpkgs> {}}:
pkgs.callPackage ../buildOpenwrt.nix {
        target = "brcm2708";
        arch = "bcm2708";
        version= "19.07.4";
        profile = "rpi";
        packages = [
            "luci-ssl"
            "luci-app-adblock"
            "luci-app-openvpn"
        ];
}
```

build an image with:

```sh
# nix-shell <path_to_device_file>.nix --run makeCleanImage
nix-shell devices/pi-img.nix --run makeCleanImage
```

For more example see `devices` folder.
