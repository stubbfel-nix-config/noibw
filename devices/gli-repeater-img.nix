{ pkgs ? import <nixpkgs> {}}:
pkgs.callPackage ../buildOpenwrt.nix {
        target = "ar71xx";
        arch = "generic";
        version= "19.07.10";
        profile = "gl-inet-6416A-v1";
        packages = [
            "luci-ssl"
            "luci-proto-wireguard"
            "luci-app-wireguard"
            "luci-app-mosquitto"
            "kmod-ath9k-htc"
            "ath9k-htc-firmware"
        ];
        moreBuildInputs = with  pkgs; [
            python2Full
        ];
}
