{ pkgs ? import <nixpkgs> {}}:
pkgs.callPackage ../buildOpenwrt.nix {
        target = "mvebu";
        arch = "cortexa9";
        version= "23.05.3";
        profile = "linksys_wrt1200ac";
        packages = [
            "adblock"
            "luci-ssl"
            "luci-proto-wireguard"
            "luci-app-wireguard"
            "luci-app-adblock"
            "luci-app-openvpn"
            "openvpn-openssl"
            "tcpdump-mini"
        ];
}
